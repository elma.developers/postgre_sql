import express, { json } from 'express'
import pg from 'pg'

const app = express();
const port = process.env.PORT || 8080

const config = {
    host: '192.168.110.107',
    user: 'postgres',
    password: 'FdsUVWRNMlYSnNne',
    database: 'elma365',
    port: 30432,
    ssl: false
};

interface IinputData {
    query: string
}


app.listen(port, () => {
    console.log("Service PostgreSQL_nodejs are listening port : " + port);
});

app.get("/api/health", (req, res) => {
    res.end("Service PostgreSQL!");
});

app.post("/api/seletRequest", express.json({ limit: '100mb' }), async (req, res) => {

    try {
        const client = new pg.Client(config);

        client.connect(err => {
            if (err) throw err;
        });
        const data = req.body as IinputData;

        const request = (await client.query(data.query)).rows ?? "";

        res.status(200).send(request);
        await client.end();
    }
    catch (err) {
        res.status(400).send("Error: " + err);
    }
});
